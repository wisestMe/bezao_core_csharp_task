﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreTask
{
    class Switch
    {
        public static void AgeGroup()
        {
            Console.WriteLine("Please how old are you?");
            string ageString = Console.ReadLine();
            //ageString = "0";
            int age = int.Parse(ageString);
            
            switch (age)
            {
                case int n when (n < 5):
                    Console.WriteLine("you are still a baby");
                    break;
                case int n when (n >= 5 && n < 13):
                    Console.WriteLine("you are just a kid");
                    break;
                case int n when (n >= 13 && n < 20):
                    Console.WriteLine("you are a teenager");
                    break;
                case int n when (n >= 20 && n < 40):
                    Console.WriteLine("you are now a man");
                    break;
                case int n when (n >= 40 && n < 60):
                    Console.WriteLine("you are a matured man");
                    break;
                case int n when (n >= 60 && n < 80):
                    Console.WriteLine("you are an advanced matured man");
                    break;
            }
        }
    }
}
