﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace CoreTask
{
    public class SixtyNumbers
    {
        public static List<int> createList()
        {
            Console.WriteLine("\n Generate a list of 60 numbers (1 - 60) using for loop");

            List<int> allNumbers = new List<int>();

            for (int i = 0; i <= 60; i++)
            {
                allNumbers.Add(i);
                //Console.WriteLine(allNumbers[i]);
            }
            //Console.WriteLine(allNumbers);
            //foreach (int aNumber in allNumbers)
            //{
            //    Console.WriteLine("yes {0}", aNumber);
            //}
            return allNumbers;
        }
    }
}
