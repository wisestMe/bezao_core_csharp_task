﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreTask
{
    class WhileLoop
    {
        public static void OddToEven()
        {
            List<int> generatedNumbers = SixtyNumbers.createList();
            Console.WriteLine("Check for odd numbers and multiply them by 2");
            foreach (int aNumber in generatedNumbers)
            {
                int Counter = aNumber;

                while(Counter % 2 == 1)
                {
                    int ConvertedNumber = aNumber * 2;
                    Console.WriteLine(ConvertedNumber);
                    Counter++;
                }
            }
        }
    }
}
