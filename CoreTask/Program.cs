﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoreTask;

namespace CoreTask
{
    class Program
    {
        static void Main()
        {
            //SixtyNumbers.createList();
            ElseIf.FizzBuzz();
            BreakContinue.BreakContinueCondition();
            Switch.AgeGroup();
            WhileLoop.OddToEven();
        }
    }
}
