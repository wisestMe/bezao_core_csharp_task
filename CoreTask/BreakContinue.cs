﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreTask
{
    class BreakContinue
    {
        public static void BreakContinueCondition()
        {
            List<int> generatedNumbers = SixtyNumbers.createList();

            Console.WriteLine("Manipulate the generated numbers (1 - 60) using Break and Continue conditions");
            Console.WriteLine("\n stop count at 40 \n skip multiples of 10");

            foreach (int aNumber in generatedNumbers)
            {
                if (aNumber == 40)
                {
                    break;
                }
                else if (aNumber % 10 == 0)
                {
                    continue;
                }
                else
                {
                    Console.WriteLine(aNumber);
                }  
            }
        }
    }
}
