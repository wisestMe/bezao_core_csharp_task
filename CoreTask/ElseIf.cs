﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreTask
{
    class ElseIf
    {
        public static void FizzBuzz()
        {
            List<int> generatedNumbers = SixtyNumbers.createList();

            Console.WriteLine("Making use of if else statements to implement FizzBuzz using numbers generated from another class");
            foreach (int aNumber in generatedNumbers)
            {
                //Console.WriteLine("yes {0}", aNumber);
                if (aNumber % 3 == 0 && aNumber % 5 == 0)
                {
                    Console.WriteLine("{0} => FizzBuzz", aNumber);
                } 
                else if (aNumber % 3 == 0)
                {
                    Console.WriteLine("{0} => Fizz", aNumber);
                }
                else if (aNumber % 5 == 0)
                {
                    Console.WriteLine("{0} => Buzz", aNumber);
                }
                
            }


        }

        
    }
}
